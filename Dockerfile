FROM alpine:latest
RUN apk add --no-cache clang-analyzer
ENTRYPOINT ["/bin/sh", "-c"]
